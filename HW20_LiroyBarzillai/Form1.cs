﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HW20_LiroyBarzillai
{
    public partial class Cards_Game : Form
    {
        private string card = "";
        private string opponentCard = "";
        private List<PictureBox> cardsArr;
        Thread myThread;

        public Cards_Game()
        {
            InitializeComponent();
            GenerateCards();
            myThread = new Thread(connect);
            myThread.Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void GenerateCards()
        {
            try
            {
                int locationX = 25;
                int locationY = 230;
                Point location = new Point(locationX, locationY);

                cardsArr = new List<PictureBox>();

                for (int i = 0; i < 10; i++)  //Creates dinamycly 10 cards 
                {
                    System.Windows.Forms.PictureBox currentPic = new PictureBox();
                    currentPic.Name = "picDynanic" + i;
                    currentPic.Image = global::HW20_LiroyBarzillai.Properties.Resources.card_back_red;
                    currentPic.Location = location;
                    currentPic.Size = new System.Drawing.Size(100, 114);
                    currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                    currentPic.Click += delegate (object sender1, EventArgs e1)
                    {
                        Random rnd = new Random();  //Generates random numbers
                        int num1 = rnd.Next(1, 14);
                        while (num1 == 10)
                        {
                            num1 = rnd.Next(1, 14);
                        }
                        int num2 = rnd.Next(1, 5);
                        string temp = "";
                        string cardName = "";

                        temp = "1";

                        if (num1 < 10)
                        {
                            temp += "0" + num1.ToString();
                        }
                        else
                        {
                            temp += num1.ToString();
                        }


                        switch (num2)
                        {
                            case 1:
                                temp += "H";
                                break;
                            case 2:
                                temp += "C";
                                break;
                            case 3:
                                temp += "S";
                                break;
                            case 4:
                                temp += "D";
                                break;
                        }

                        cardName = resolveNameOfPic(num1, num2);
                        card = temp;
                        cardName += ".png";
                        cardName = Directory.GetCurrentDirectory() + @"\Resources\" + cardName;
                        currentPic.Image = Image.FromFile(cardName);  //Loads the random image

                    };


                    this.Controls.Add(currentPic);

                    cardsArr.Add(currentPic);

                    location.X += currentPic.Size.Width + 5;
                }

                location.X = 495;
                location.Y = 75;

                System.Windows.Forms.PictureBox blueCardPic = new PictureBox();
                blueCardPic.Name = "picDynanic" + 10;
                blueCardPic.Image = global::HW20_LiroyBarzillai.Properties.Resources.card_back_blue;
                blueCardPic.Location = location;
                blueCardPic.Size = new System.Drawing.Size(100, 114);
                blueCardPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                blueCardPic.Click += delegate (object sender1, EventArgs e1)
                {
                    
                };

                this.Controls.Add(blueCardPic);
                cardsArr.Add(blueCardPic);
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }

        }

        //The function connects to the server
        private void connect()
        {
            try
            {
                int num1 = 0;

                TcpClient client = new TcpClient();
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
                client.Connect(serverEndPoint);
                NetworkStream clientStream = client.GetStream();

                freeze();

                byte[] bufferIn1 = new byte[4];
                int bytesRead3 = clientStream.Read(bufferIn1, 0, 4);
                string input = new ASCIIEncoding().GetString(bufferIn1);

                release();

                while (true)
                {
                    while (card.Equals("")) //Checks if the username picked a card
                    {
                    }

                    freeze();

                    byte[] buffer = new ASCIIEncoding().GetBytes(card);
                    clientStream.Write(buffer, 0, 4);
                    clientStream.Flush();



                    byte[] bufferIn = new byte[4];
                    int bytesRead = clientStream.Read(bufferIn, 0, 4);
                    opponentCard = new ASCIIEncoding().GetString(bufferIn);  //Gets th card of the opponent

                    switch (opponentCard.Substring(3))
                    {
                        case "H":
                            num1 = 1;
                            break;
                        case "C":
                            num1 = 2;
                            break;
                        case "S":
                            num1 = 3;
                            break;
                        case "D":
                            num1 = 4;
                            break;
                    }

                    string cardName = resolveNameOfPic(Convert.ToInt32(opponentCard.Substring(1, 2)), num1);
                    cardName += ".png";
                    cardName = Directory.GetCurrentDirectory() + @"\Resources\" + cardName;
                    cardsArr[10].Image = Image.FromFile(cardName);

                    findWinner();
                    card = "";
                    opponentCard = "";

                    init();
                    release();
                }
            }
            catch (Exception e2)
            {
                MessageBox.Show(e2.Message);
            }

        }

        //Event of closing the program
        void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                MessageBox.Show("The result is " + lbl_myScore.Text + " - " + lbl_opponentScore.Text);
                myThread.Abort();
                Environment.Exit(0);
            }
            catch (Exception e3)
            {
                MessageBox.Show(e3.Message);
            }
        }
        
        //The function initiates all the cards (shows their back)
        private void init()
        {
            try
            {
                for (int i = 0; i < cardsArr.Count; i++)
                {
                    cardsArr[i].Image = global::HW20_LiroyBarzillai.Properties.Resources.card_back_red;
                }

                cardsArr[10].Image = global::HW20_LiroyBarzillai.Properties.Resources.card_back_blue;
            }
            catch (Exception e4)
            {
                MessageBox.Show(e4.Message);
            }
        }

        //The function makes that all the cards will be unclicked
        private void freeze()
        {
            try
            {
                for (int i = 0; i < cardsArr.Count; i++)
                {
                    Invoke((MethodInvoker)delegate { cardsArr[i].Enabled = false; });
                }
            }
            catch (Exception e5)
            {
                MessageBox.Show(e5.Message);
            }
        }

        //The function makes that all the cards could be clicked
        private void release()
        {
            try
            {
                for (int i = 0; i < cardsArr.Count; i++)
                {
                    Invoke((MethodInvoker)delegate { cardsArr[i].Enabled = true; });
                }
            }
            catch (Exception e6)
            {
                MessageBox.Show(e6.Message);
            }
        }

        //The function gets two numbers and checks which card is picked
        private string resolveNameOfPic(int num1, int num2)
        {

            string cardName = "";

            try
            {

                switch (num1)
                {
                    case 1:
                        cardName = "ace_of_";
                        break;
                    case 2:
                        cardName = "2_of_";
                        break;
                    case 3:
                        cardName = "3_of_";
                        break;
                    case 4:
                        cardName = "4_of_";
                        break;
                    case 5:
                        cardName = "5_of_";
                        break;
                    case 6:
                        cardName = "6_of_";
                        break;
                    case 7:
                        cardName = "7_of_";
                        break;
                    case 8:
                        cardName = "8_of_";
                        break;
                    case 9:
                        cardName = "9_of_";
                        break;
                    case 11:
                        cardName = "jack_of_";
                        break;
                    case 12:
                        cardName = "queen_of_";
                        break;
                    case 13:
                        cardName = "king_of_";
                        break;
                }

                switch (num2)
                {
                    case 1:
                        cardName += "hearts";
                        break;
                    case 2:
                        cardName += "clubs";
                        break;
                    case 3:
                        cardName += "spades";
                        break;
                    case 4:
                        cardName += "diamonds";
                        break;
                }

                
            }
            catch (Exception e9)
            {
                MessageBox.Show(e9.Message);
            }

            return cardName;
        }

        //The function checks who is the wiiner of the round and changes the score accordingly
        private void findWinner()
        {
            try
            {
                int myScore = Convert.ToInt32(card.Substring(1, 2));
                int opponentScore = Convert.ToInt32(opponentCard.Substring(1, 2));

                if (myScore > opponentScore)
                {
                    MessageBox.Show("You won this round!");
                    Invoke((MethodInvoker)delegate { lbl_myScore.Text = (Convert.ToInt32(lbl_myScore.Text) + 1).ToString(); });
                }
                else if (opponentScore > myScore)
                {
                    MessageBox.Show("You lost this round!");
                    Invoke((MethodInvoker)delegate { lbl_opponentScore.Text = (Convert.ToInt32(lbl_opponentScore.Text) + 1).ToString(); });
                }
                else
                {
                    MessageBox.Show("It's a tie!");
                }
            }
            catch (Exception e7)
            {
                MessageBox.Show(e7.Message);
            }
        }

        //Event of forfeit button click
        private void btn_forfeit_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show("The result is " + lbl_myScore.Text + " - " + lbl_opponentScore.Text);
                myThread.Abort();
                Environment.Exit(0);
            }
            catch (Exception e8)
            {
                MessageBox.Show(e8.Message);
            }
        }
    }
}
