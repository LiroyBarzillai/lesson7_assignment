﻿using System.Windows.Forms;

namespace HW20_LiroyBarzillai
{
    partial class Cards_Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_forfeit = new System.Windows.Forms.Button();
            this.my_score = new System.Windows.Forms.Label();
            this.lbl_myScore = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_opponentScore = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_forfeit
            // 
            this.btn_forfeit.Location = new System.Drawing.Point(507, 33);
            this.btn_forfeit.Name = "btn_forfeit";
            this.btn_forfeit.Size = new System.Drawing.Size(75, 23);
            this.btn_forfeit.TabIndex = 0;
            this.btn_forfeit.Text = "Forfeit";
            this.btn_forfeit.UseVisualStyleBackColor = true;
            this.btn_forfeit.Click += new System.EventHandler(this.btn_forfeit_Click);
            // 
            // my_score
            // 
            this.my_score.AutoSize = true;
            this.my_score.Location = new System.Drawing.Point(12, 9);
            this.my_score.Name = "my_score";
            this.my_score.Size = new System.Drawing.Size(61, 13);
            this.my_score.TabIndex = 1;
            this.my_score.Text = "Your score:";
            // 
            // lbl_myScore
            // 
            this.lbl_myScore.AutoSize = true;
            this.lbl_myScore.Location = new System.Drawing.Point(79, 9);
            this.lbl_myScore.Name = "lbl_myScore";
            this.lbl_myScore.Size = new System.Drawing.Size(13, 13);
            this.lbl_myScore.TabIndex = 2;
            this.lbl_myScore.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(957, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Opponent\'s score:";
            this.label1.Click += new System.EventHandler(this.label3_Click);
            // 
            // lbl_opponentScore
            // 
            this.lbl_opponentScore.AutoSize = true;
            this.lbl_opponentScore.Location = new System.Drawing.Point(1057, 9);
            this.lbl_opponentScore.Name = "lbl_opponentScore";
            this.lbl_opponentScore.Size = new System.Drawing.Size(13, 13);
            this.lbl_opponentScore.TabIndex = 4;
            this.lbl_opponentScore.Text = "0";
            // 
            // Cards_Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.ClientSize = new System.Drawing.Size(1097, 377);
            this.Controls.Add(this.lbl_opponentScore);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl_myScore);
            this.Controls.Add(this.my_score);
            this.Controls.Add(this.btn_forfeit);
            this.Name = "Cards_Game";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

            this.FormClosed += new FormClosedEventHandler(Form1_FormClosed);

        }

        #endregion

        private System.Windows.Forms.Button btn_forfeit;
        private System.Windows.Forms.Label my_score;
        private System.Windows.Forms.Label lbl_myScore;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_opponentScore;
    }
}

